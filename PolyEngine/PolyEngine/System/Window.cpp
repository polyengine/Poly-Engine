#include "Window.hpp"
#include <iostream>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image/stb_image.h>

void APIENTRY glDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam);
poly::system::Window::Window()
{
}

poly::system::Window::~Window()
{
}

void poly::system::Window::createWindow(std::string title, int width, int height)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
	m_pWindow = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
	if(m_pWindow == nullptr)
	{
		std::cout << "Failed to initialize GLFW\n";
		glfwTerminate();
		return;
	}
	glfwMakeContextCurrent(m_pWindow);
	if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD\n";
		glfwTerminate();
		return;
	}
	GLint flags; 
	glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
	{
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(glDebugOutput, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
}

void poly::system::Window::setClearColor(glm::vec4 color)
{
	glClearColor(color.x, color.y, color.z, color.w);
}

void poly::system::Window::setWindowIcon(std::string iconPath)
{
    // should be a 16 and 32 bit icon
	GLFWimage image;
	int width, height, bbp;
	unsigned char* rgb = stbi_load(iconPath.c_str(), &width, &height, &bbp, 4);
	image.width = width;
	image.height = height;
	image.pixels = rgb;
	glfwSetWindowIcon(m_pWindow, 1, &image);
    stbi_image_free(rgb);
}

void poly::system::Window::setKeyboardCallback(GLFWkeyfun keyFunction)
{
	glfwSetKeyCallback(m_pWindow, keyFunction);
}

void poly::system::Window::setMousePosCallback(GLFWcursorposfun posFunction)
{
	glfwSetCursorPosCallback(m_pWindow, posFunction);
}

void poly::system::Window::setMouseClickCallback(GLFWmousebuttonfun buttonFunction)
{
	glfwSetMouseButtonCallback(m_pWindow, buttonFunction);
}

void poly::system::Window::setMouseScrollCallback(GLFWscrollfun scrollFunction)
{
	glfwSetScrollCallback(m_pWindow, scrollFunction);
}

void poly::system::Window::setJoystickCallback(GLFWjoystickfun joystickFunction)
{

}

bool poly::system::Window::isOpen()
{
	return static_cast<bool>(glfwWindowShouldClose(m_pWindow));
}

void poly::system::Window::processEvents()
{
	glfwPollEvents();
}

void poly::system::Window::display()
{
	glfwSwapBuffers(m_pWindow);
}

void  APIENTRY glDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

	std::cout << "---------------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;

	switch (source)
	{
	case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
	} std::cout << std::endl;

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	} std::cout << std::endl;

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:         std::cout << "Severity: high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
	} std::cout << std::endl;
	std::cout << std::endl;
}