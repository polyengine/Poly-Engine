#pragma once
#include <string>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

namespace poly
{
	namespace system
	{
		class Window
		{
		public:
			Window();
			~Window();
			void createWindow(std::string title, int width, int height);
			void setClearColor(glm::vec4 color);
			void setWindowIcon(std::string iconPath);
			void setKeyboardCallback(GLFWkeyfun keyFunction);
			void setMousePosCallback(GLFWcursorposfun posFunction);
			void setMouseClickCallback(GLFWmousebuttonfun buttonFunction);
			void setMouseScrollCallback(GLFWscrollfun scrollFunction);
			void setJoystickCallback(GLFWjoystickfun joystickFunction);
            bool isOpen();
            void processEvents();
            void display();
		private:
			GLFWwindow * m_pWindow;
		};
	};
};