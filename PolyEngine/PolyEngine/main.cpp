#include <vector>
#include "System/Window.hpp"
#include "System/Shader.hpp"
#include "Graphics/Vertex.hpp"
int main()
{
	poly::system::Window window;
	window.createWindow("Hello OpenGL", 800, 600);
	window.setClearColor({ .2f, .6f, .6f, 1.0f });
	window.setWindowIcon("Res/Icons/vk.png");

	glm::vec3 vertices[] = {glm::vec3(-1,  1,  1),
                            glm::vec3(0,  1,  1),
                            glm::vec3(-1, -1,  1),
                            glm::vec3(0, -1,  1)};
	std::vector<poly::graphics::Vertex> verticess;
	for (auto vertex : vertices)
	{
		poly::graphics::Vertex vert;
		vert.Position = vertex;
		verticess.push_back(vert);
	}
	GLuint indices[] = {0, 1, 2, 2, 1, 3};
	GLuint vao, vbo, ebo;
	
	glCreateBuffers(1, &vbo);	// vertex buffer
	glNamedBufferStorage(vbo, verticess.size() * sizeof(verticess[0]), verticess.data(), GL_DYNAMIC_STORAGE_BIT);

	glCreateBuffers(1, &ebo);	// ebo
	glNamedBufferStorage(ebo, sizeof(indices), indices, GL_DYNAMIC_STORAGE_BIT);

	glCreateVertexArrays(1, &vao); // vertex array object

	glVertexArrayVertexBuffer(vao, 0, vbo, 0, sizeof(poly::graphics::Vertex));
	glVertexArrayElementBuffer(vao, ebo);

	glEnableVertexArrayAttrib(vao, 0); // vao, index
	
	glVertexArrayAttribFormat(vao, 0, 3, GL_FLOAT, GL_FALSE, offsetof(poly::graphics::Vertex, Position));
	
	glVertexArrayAttribBinding(vao, 0, 0); 

	poly::system::Shader shaders;
	shaders.addShader("Shaders/BasicShape.vert", GL_VERTEX_SHADER);
	shaders.addShader("Shaders/BasicShape.frag", GL_FRAGMENT_SHADER);
	shaders.compileProgram();

	shaders.useProgram();
	glm::vec4 col(.2f, .2f, .2f, 0.0f);
	// glUniform4fv(0, 1, glm::value_ptr(col));
	shaders.unuseProgram();
	while(!window.isOpen())
    {
        window.processEvents();
        glClear(GL_COLOR_BUFFER_BIT);
        shaders.useProgram();
        glBindVertexArray(vao);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
        glBindVertexArray(0);
        window.display();
    }
	return 1;
}